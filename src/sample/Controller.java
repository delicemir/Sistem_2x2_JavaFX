package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public TextField jed1;
    public TextField jed2;
    public TextField result;
    public Button calc;

    private double a1,b1,c1;
    private double a2,b2,c2;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        calc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText("Calculating...");
                calculate();
            }

        });

    }

    private boolean setParamJed1() {
        String test = jed1.getText().replace(" ",""); //ukloni sva prazna mjesta
        test=test.toUpperCase();                                        // pretvori sve u velika slova
        if(test.contains("X")&&test.contains("Y")&&test.contains("=")&&(test.contains("+")||test.contains("-"))) {
            //koeficijent a1
            if (test.substring(0, test.indexOf('X')).length() == 0) {
                this.a1 = 1.0;
            }
            else if(test.substring(0,1).contains("-")&&(test.substring(0,test.indexOf('X')).length()==1)) {
                this.a1=-1.0;
            }
            else {
                this.a1 = Double.parseDouble(test.substring(0, test.indexOf('X')));
            }
            //koeficijent b1
            if((test.substring(test.indexOf('X') + 1, test.indexOf('Y')).contains("+"))) {
                if(test.substring(test.indexOf('X') + 1, test.indexOf('Y')).length()>1) {
                    this.b1 =Double.parseDouble(test.substring(test.indexOf('X') + 2, test.indexOf('Y')));
                }
                else{
                    this.b1=1.0;
                }
            }

            if((test.substring(test.indexOf('X') + 1, test.indexOf('Y')).contains("-"))) {
                if(test.substring(test.indexOf('X') + 1, test.indexOf('Y')).length()>1) {
                    this.b1 =(-1.0)*Double.parseDouble(test.substring(test.indexOf('X') + 2, test.indexOf('Y')));
                }
                else{
                    this.b1=-1.0;
                }
            }
            //koeficijent c1
            if(test.substring(test.indexOf('=') + 1, test.length()).length()!=0) {
                this.c1 = Double.parseDouble(test.substring(test.indexOf('=') + 1, test.length()));
            }
            else {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    private boolean setParamJed2() {
        String test = jed2.getText().replace(" ","");  //ukloni sva prazna mjesta
        test=test.toUpperCase();                                        // pretvori sve u velika slova
        if(test.contains("X")&&test.contains("Y")&&test.contains("=")&&(test.contains("+")||test.contains("-"))) {
            //koeficijent a2
            if (test.substring(0, test.indexOf('X')).length() == 0) {
                this.a2 = 1.0;
            }
            else if(test.substring(0,1).contains("-")&&(test.substring(0,test.indexOf('X')).length()==1)) {
                this.a2 = -1.0;
            }
            else {
                this.a2 = Double.parseDouble(test.substring(0, test.indexOf('X')));
            }
            //koeficijent b2
            if((test.substring(test.indexOf('X') + 1, test.indexOf('Y')).contains("+"))) {
                if(test.substring(test.indexOf('X') + 1, test.indexOf('Y')).length()>1) {
                    this.b2 =Double.parseDouble(test.substring(test.indexOf('X') + 2, test.indexOf('Y')));
                }
                else{
                    this.b2=1.0;
                }
            }

            if((test.substring(test.indexOf('X') + 1, test.indexOf('Y')).contains("-"))) {
                if(test.substring(test.indexOf('X') + 1, test.indexOf('Y')).length()>1) {
                    this.b2 =(-1.0)*Double.parseDouble(test.substring(test.indexOf('X') + 2, test.indexOf('Y')));
                }
                else{
                    this.b2=-1.0;
                }
            }
            //koeficijent c2
            if(test.substring(test.indexOf('=') + 1, test.length()).length()!=0) {
                this.c2 = Double.parseDouble(test.substring(test.indexOf('=') + 1, test.length()));
            }
            else {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    private double dx() {
        return c1*b2-c2*b1;
    }
    private double dy() {
        return a1*c2-a2*c1;
    }

    private double det() {
        return a1*b2-a2*b1;
    }

    private void calculate() {

        if(jed1.getText().isEmpty() || jed2.getText().isEmpty()) {
            result.setText("Niste upisali jednačine sistema!");
        }
        else {

            double x = 0;
            double y = 0;

            try {
                if(!setParamJed1()&& !setParamJed2()) {
                    result.setText("Neispravno upisane obje jednačine !");
                }
                else if(!setParamJed2()) {
                    result.setText("Neispravno upisana jednačina 2 !");
                }
                else if(!setParamJed1()) {
                    result.setText("Neispravno upisana jednačina 1 !");
                }
                else{
                    if (det() != 0) {
                        x = dx() / det();
                        y = dy() / det();
                        result.setText(String.format("(X: %.4f)  (Y: %.4f)", x, y));
                        printCoeffJed1();
                        printCoeffJed2();
                    } else if ((dx() == 0 || dy() == 0) && det() == 0) {
                        result.setText("Beskonačno mnogo rješenja!");
                    } else {
                        result.setText("Nemoguć sistem, nema rješenja!");
                    }
                }

            } catch (Exception e) {
                System.out.println("Error: " + e);
            }
        }

    }


    private void printCoeffJed1() {
        System.out.println("Koeficijenti prve jednačine su: ");
        System.out.println("a1: "+this.a1);
        System.out.println("b1: "+this.b1);
        System.out.println("c1: "+this.c1);
        System.out.println("********************************");
    }

    private void printCoeffJed2() {
        System.out.println("Koeficijenti druge jednačine su: ");
        System.out.println("a2: "+this.a2);
        System.out.println("b2: "+this.b2);
        System.out.println("c2: "+this.c2);
        System.out.println("********************************");
    }

}
